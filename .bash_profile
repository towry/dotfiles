# clear screen
alias cls=clear

# Rvm
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
source ~/.profile

# Quick set terminal title
function title {
	printf "\033]0;%s\007" "$1"
}

# Git
export PATH=/usr/local/git/bin:/usr/local/sbin:$PATH

function tree() {
  find . -print | grep -v .git | sed -e 's;[^/]*/;|____;g;s;____|; |;g' | less
}

